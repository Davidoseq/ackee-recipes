import React from 'react';
import { connect } from 'react-redux';
import { createRecipe } from '../../actions';
import RecipeForm from '../RecipeForm/RecipeForm';
import Bar from '../Bar/Bar';
import {setCreatedRecipeByUser} from '../../services/createdRecipeByUser'

class RecipeCreate extends React.Component {
    onSubmit = async formValues => {
        const createdRecipe = await this.props.createRecipe(formValues);

        if (typeof createdRecipe.data.id !== 'undefined') {
            this.props.history.push(`/detail/${createdRecipe.data.id}`)
            setCreatedRecipeByUser(createdRecipe.data.id)
        }
    };

    render() {
        return (
            <React.Fragment>
                <Bar showPreviousButton={true} title="Přidat recept" isCreateRecipePage={true}/>
                <section className="Section">
                    <div className="Container Container--biggerOffset">
                        <RecipeForm errorMessage={this.props.errorMessage} onSubmit={this.onSubmit} />
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errorMessage: Object.values(state.error),
    }
};

export default connect(
    mapStateToProps,
    { createRecipe },
)(RecipeCreate);
