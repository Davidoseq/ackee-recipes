import React from 'react';
import { connect } from 'react-redux';
import { deleteRecipe, fetchRecipeDetail } from '../../actions';
import './RecipeDetail.scss';
import Bar from '../Bar/Bar';
import Rate from '../Rate/Rate';
import Duration from '../Duration/Duration';
import { isRecipeCreatedByUser } from '../../services/createdRecipeByUser';

class RecipeDetail extends React.Component {
    componentDidMount() {
        this.fetchRecipeDetail();
    }

    fetchRecipeDetail() {
        this.props.fetchRecipeDetail(this.props.match.params.id);
    }

    renderIngredients(ingredients) {
        return (
            <ul>
                {ingredients.map((ingredient, index) => {
                    return <li key={index}>{ingredient}</li>;
                })}
            </ul>
        );
    }

    async removeRecipe(id) {
        const deletedRecipe = await this.props.deleteRecipe(id);
        if (deletedRecipe.status === 204) {
            this.props.history.push('/');
        }
    }

    render() {
        const { name, description, duration, info, ingredients, score } = this.props.currentRecipe;
        const { id } = this.props.match.params;

        return (
            <React.Fragment>
                <Bar showPreviousButton={true} isRecipeDetail={true} inverted />

                <div className="RecipeHeaderImage">
                    <div className="RecipeHeader-overlay">
                        <img src="/images/recipe-placeholder.png" alt="" />
                    </div>
                    <div className="RecipeHeaderImage-content">
                        <div className="Container Container--biggerOffset">
                            <h1 className="RecipeHeaderImage-title">{name}</h1>
                        </div>
                        <div className="RecipeHeaderImage-additional">
                            <div className="Container Container--biggerOffset">
                                <div className="Row u-flex-alignItemsCenter">
                                    <div className="Row-left">
                                        <Rate
                                            fill="#ffffff"
                                            size="medium"
                                            readonly
                                            rate={score}
                                            style={{ opacity: 0.9 }}
                                        />
                                    </div>
                                    <div className="Row-right">
                                        <Duration time={duration} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="Section u-colorGray">
                    <div className="Container Container--biggerOffset">
                        <p className="Section">{info}</p>
                        <h2 className="Heading Heading--2">Ingredience</h2>

                        {ingredients && this.renderIngredients(ingredients)}

                        <div className="Section">
                            <h2 className="Heading Heading--2">Příprava jídla</h2>
                            <p>{description}</p>
                        </div>
                        {isRecipeCreatedByUser(id) && (
                            <button className="Button Button--outline" onClick={() => this.removeRecipe(id)}>
                                Smazat recept
                            </button>
                        )}
                    </div>
                </div>

                <section className="Section Section--big u-backgroundBlue u-colorWhite u-textCenter">
                    <h2 className="Heading Heading--2 u-colorWhite u-pb--6">Ohodnoť tento recept</h2>
                    <Rate id={id} size="big" fill="#ffffff" updateComponent={() => this.fetchRecipeDetail()} />
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return { currentRecipe: state.currentRecipe };
};

export default connect(
    mapStateToProps,
    { fetchRecipeDetail, deleteRecipe },
)(RecipeDetail);
