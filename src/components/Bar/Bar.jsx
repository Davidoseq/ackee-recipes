import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import RecipeFormRemoteSubmit from '../RecipeForm/RecipeFormRemoteSubmit';
import './Bar.scss';
import { ReactComponent as PlusIcon } from '../../assets/svg/plus.svg';
import { ReactComponent as BackIcon } from '../../assets/svg/arrow-left.svg';

const Bar = ({ showPreviousButton, title, isRecipeDetail, isCreateRecipePage, inverted }) => {
    const isInverted = inverted => {
        return inverted ? '#ffffff' : '#0000ff';
    };

    return (
        <header className={`Bar ${isRecipeDetail ? `Bar--recipeDetail` : ``}`}>
            <div className="Container">
                <div className="Row u-flex-alignItemsCenter">
                    <div className="Row-left">
                        {showPreviousButton && (
                            <span className="Bar-previous">
                                <Link to="/" className="Button">
                                    <BackIcon fill={isInverted(inverted)} />
                                </Link>
                            </span>
                        )}
                        {title && <span className="Bar-content">{title}</span>}
                    </div>
                    <div className="Row-right">
                        {isCreateRecipePage ? (
                            <RecipeFormRemoteSubmit />
                        ) : (
                            <Link to="/add" className="Button">
                                <PlusIcon fill={isInverted(inverted)} />
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        </header>
    );
};

export default connect()(Bar);
