import React from 'react';
import './Duration.scss';
import { ReactComponent as ClockIcon } from '../../assets/svg/clock.svg'

const Duration = ({ time }) => {
    return (
        <div className="Duration">
            <span className="Icon Icon--inText">
                <ClockIcon/>
            </span>
            <span className="Duration-text">{time} min</span>
        </div>
    );
};

export default Duration;
