import React from 'react';
import { connect } from 'react-redux';
import RecipeCard from '../RecipeCard/RecipeCard';
import { fetchRecipeList } from '../../actions';
import Bar from '../Bar/Bar';

class HomePage extends React.Component {
    componentDidMount() {
        return this.props.fetchRecipeList();
    }

    renderRecipeList() {
        return this.props.recipes.map(recipe => {
            return (
                recipe.id && (
                    <RecipeCard
                        key={recipe.id}
                        id={recipe.id}
                        name={recipe.name}
                        time={recipe.duration}
                        rating={recipe.score}
                    />
                )
            );
        });
    }

    render() {
        return (
            <React.Fragment>
                <Bar showPreviousButton={false} title="Recepty" />

                <div className="Container">
                    <div className="Section">{this.props.recipes && this.renderRecipeList()}</div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return { recipes: Object.values(state.recipes) };
};

export default connect(
    mapStateToProps,
    { fetchRecipeList },
)(HomePage);
