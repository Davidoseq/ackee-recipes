import React from 'react';
import './RecipeCard.scss';
import Duration from '../Duration/Duration';
import Rate from '../Rate/Rate';
import { Link } from 'react-router-dom';

const RecipeCard = ({ id, name, time, rating }) => {
    return (
        <Link to={`/detail/${id}`} className="RecipeCard">
            <div className="Media">
                <div className="Media-object">
                    <img src="/images/recipe-placeholder.png" alt="" width="96" height="96" className="u-roundImage" />
                </div>
                <div className="Media-body">
                    <h2 className="RecipeCard-title">{name}</h2>

                    <div className="RecipeCard-stars">
                        <Rate fill="#ff00ff" size="small" readonly rate={rating} />
                    </div>

                    <Duration time={time} />
                </div>
            </div>
        </Link>
    );
};
export default RecipeCard;
