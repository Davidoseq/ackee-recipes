import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import {ReactComponent as PlusIcon} from "../../assets/svg/plus.svg";

const RemoteSubmitButton = ({ dispatch }) => (
    <button className='Button' type="button" onClick={() => dispatch(submit('recipeForm'))}>
        <PlusIcon fill='#0000ff' />
    </button>
);

export default connect()(RemoteSubmitButton);
