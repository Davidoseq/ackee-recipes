import React from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import './RecipeForm.scss';
import validate from './validate';
import { ReactComponent as PlusIcon } from '../../assets/svg/plus.svg';

class RecipeForm extends React.Component {
    renderField = ({ input, label, type, meta: { touched, error }, className }) => (
        <div className={`Control ${className}`}>
            <input {...input} id={input.name} type={type} className="Control-input" required />
            <label htmlFor={input.name} className="Control-label">
                {label}
            </label>

            {touched && error && <span className="Control-error">{error}</span>}
        </div>
    );

    renderIngredients = ({ fields, meta: { error } }) => (
        <React.Fragment>
            <h2 className="Heading Heading--4 u-upperCase">Ingredience</h2>

            {fields.map((ingredient, index) => (
                <div key={index} className="u-positionRelative">
                    <Field
                        name={ingredient}
                        type="text"
                        component={this.renderField}
                        label={`Vaše ingredience`}
                        className="Control-smallSpace Control--hideLabel"
                    />
                    {index >= 1 && (
                        <button
                            className="Control-removeField"
                            type="button"
                            title="Odstranit ingredienci"
                            onClick={() => fields.remove(index)}
                        >
                            <span role="img" aria-label="Remove field">
                                ❌
                            </span>
                        </button>
                    )}
                </div>
            ))}
            {error && <li className="error">{error}</li>}

            <div className="Control">
                <button className="Button Button--outline u-upperCase" type="button" onClick={() => fields.push()}>
                    <PlusIcon fill="#ff00ff" width="8" />
                    <span className="Button-text">Přidat</span>
                </button>
            </div>
        </React.Fragment>
    );

    onSubmit = formValues => {
        this.props.onSubmit(formValues);
    };

    render() {
        const { submitting, errorMessage, handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit)} noValidate>
                <div className="Control-group">
                    <Field name="name" component={this.renderField} label="Název receptu" />
                    <Field name="info" component={this.renderField} label="Úvodní text" />
                </div>
                <div className="Control-group">
                    <FieldArray name="ingredients" component={this.renderIngredients} />
                </div>
                <div className="Control-group">
                    <Field name="description" component={this.renderField} label="Postup" />
                    <Field name="duration" component={this.renderField} label="Čas" />
                </div>
                {errorMessage && (
                    <div className="Control-group">
                        <strong>{errorMessage}</strong>
                    </div>
                )}
                <div>
                    <button type="submit" disabled={submitting} className="Button Button--outline u-upperCase">
                        <span className="Button-text">Přidat recept</span>
                    </button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'recipeForm',
    initialValues: {
        ingredients: [''],
    },
    validate,
})(RecipeForm);
