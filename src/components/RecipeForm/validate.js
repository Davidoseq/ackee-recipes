const validate = formValues => {
    const errors = {};
    errors.ingredients = {};

    if (!formValues.name) {
        errors.name = 'Prosím vyplňte název receptu';
    }

    if (formValues.name && !formValues.name.toLowerCase().match('ackee')) {
        errors.name = 'Chybí mi tu kouzelné slovíčko Ackee';
    }

    if (!formValues.description) {
        errors.description = 'Prosím vyplňte úvodní text receptu';
    }

    if (!formValues.info) {
        errors.info = 'Prosím vyplňte postup receptu';
    }

    if (!formValues.duration) {
        errors.duration = 'Prosím vyplňte čas receptu';
    }

    if (formValues.duration && isNaN(formValues.duration)) {
        errors.duration = 'Čas musí být číslo';
    }

    if (formValues.duration && formValues.duration < 0) {
        errors.duration = 'Záporný čas u receptu, to je mi zajimavé. Zkusíme to znova?';
    }

    if (typeof formValues.ingredients !== 'undefined') {
        formValues.ingredients.forEach((ingredient, ingredientIndex) => {
            if (!ingredient) {
                errors.ingredients[ingredientIndex] = 'Prosím vyplňte ingredienci';
            }
        });
    }

    return errors;
};

export default validate
