import React from 'react';
import './Star.scss';
import { ReactComponent as StarIcon } from '../../assets/svg/star.svg';
import PropTypes from 'prop-types';

const Star = ({ fill, size = 'medium', style, readonly, value, hoverValue, setHoverValue, sendRating, totalRate }) => {
    const sizes = {
        small: 12,
        medium: 24,
        big: 36,
    };

    const getClassName = (size, readonly) => {
        let className = 'Star';

        className += ` Star--${size} `;
        className += readonly ? ` Star--full Star--disabled` : ``;
        if (!readonly) {
            className += hoverValue >= value ? ` Star--full` : ` Star--empty`;
        }

        return className;
    };

    const mouseEnter = () => {
        setHoverValue(value);
    };

    const mouseLeave = () => {
        setHoverValue(0);
    };

    const handleClick = () => {
        sendRating(value);
    };

    const getTitle = (readonly, value, totalRate) => {
        if (readonly) {
            return totalRate;
        }

        return value;
    };

    return (
        <StarIcon
            onMouseEnter={mouseEnter}
            onMouseLeave={mouseLeave}
            onClick={handleClick}
            fill={fill}
            width={sizes[size]}
            height={sizes[size]}
            value={value}
            title={getTitle(readonly, value, totalRate)}
            className={getClassName(size, readonly)}
            style={style}
        />
    );
};

Star.propTypes = {
    fill: PropTypes.string,
    size: PropTypes.string,
    style: PropTypes.object,
    readonly: PropTypes.bool,
    value: PropTypes.number,
    hoverValue: PropTypes.number,
    setHoverValue: PropTypes.func,
    sendRating: PropTypes.func,
    totalRate: PropTypes.number,
};

export default Star;
