import React from 'react';
import recipes from '../../apis/recipes';
import Star from '../Star/Star';
import { setRatedRecipe, isRecipeRated } from '../../services/ratedRecipes';

class Rate extends React.Component {
    state = {
        hoverValue: 0,
        ratingSubmitted: isRecipeRated(this.props.id),
    };

    setHoverValue = value => {
        this.setState({ hoverValue: value });
    };

    sendRating = async score => {
        const response = await recipes.post(`/recipes/${this.props.id}/ratings`, { score: score });
        if (response.status === 200) {
            this.setState({ ratingSubmitted: true });
            this.props.updateComponent();
            setRatedRecipe(this.props.id);
        }
    };

    renderStars = (rate = 5, size, fill, style, readonly) => {
        const count = Math.round(rate);
        const stars = [];

        for (let value = 1; value <= count; value++) {
            stars.push(
                <Star
                    fill={fill}
                    size={size}
                    style={style}
                    readonly={readonly}
                    value={value}
                    key={value}
                    setHoverValue={this.setHoverValue}
                    sendRating={this.sendRating}
                    hoverValue={this.state.hoverValue}
                    totalRate={rate}
                />,
            );
        }

        return stars;
    };

    render() {
        const { fill, size, style, readonly, rate } = this.props;

        return (
            <React.Fragment>
                {this.state.ratingSubmitted && !readonly ? (
                    <h3 className="Heading Heading--3 u-colorWhite">
                        Díky za ohodnocení
                        <span role="img" aria-label="thank you">
                            &nbsp;🎉
                        </span>
                    </h3>
                ) : (
                    this.renderStars(rate, size, fill, style, readonly)
                )}
            </React.Fragment>
        );
    }
}

export default Rate;
