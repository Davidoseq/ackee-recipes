import Cookies from 'js-cookie';

export const setCreatedRecipeByUser = id => {
    const ratedRecipes = new Set(getCreatedRecipesByUser());
    ratedRecipes.add(id);
    Cookies.set('createdRecipesByUser', [...ratedRecipes]);
};

export const getCreatedRecipesByUser = () => {
    return Cookies.getJSON('createdRecipesByUser');
};

export const isRecipeCreatedByUser = id => {
    if (typeof Cookies.getJSON('createdRecipesByUser') !== 'undefined') {
        return Cookies.getJSON('createdRecipesByUser').includes(id);
    }
    return false;
};
