import Cookies from 'js-cookie';

export const setRatedRecipe = id => {
    const ratedRecipes = new Set(getRatedRecipes());
    ratedRecipes.add(id);
    Cookies.set('ratedRecipes', [...ratedRecipes]);
};

export const getRatedRecipes = () => {
    return Cookies.getJSON('ratedRecipes');
};

export const isRecipeRated = (id) => {
    if (typeof Cookies.getJSON('ratedRecipes') !== 'undefined') {
        return Cookies.getJSON('ratedRecipes').includes(id);
    }
};
