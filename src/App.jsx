import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import HomePage from './components/HomePage/HomePage';
import RecipeDetail from './components/RecipeDetail/RecipeDetail';
import RecipeCreate from './components/RecipeCreate/RecipeCreate';

const App = () => {
    return (
        <div>
            <Router>
                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/detail/:id" exact component={RecipeDetail} />
                    <Route path="/add" exact component={RecipeCreate} />
                    <Route>404</Route>
                </Switch>
            </Router>
        </div>
    );
};

export default App;
