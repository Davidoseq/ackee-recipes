import recipes from '../apis/recipes';

import { FETCH_RECIPE_LIST, FETCH_RECIPE_DETAIL, CREATE_RECIPE, ERROR, DELETE_RECIPE } from './types';

export const fetchRecipeList = () => async dispatch => {
    const response = await recipes.get('/recipes', {
        'Content-Type': 'application/json',
    });

    dispatch({ type: FETCH_RECIPE_LIST, payload: response.data });
};

export const fetchRecipeDetail = id => async dispatch => {
    const response = await recipes.get(`/recipes/${id}`);

    dispatch({ type: FETCH_RECIPE_DETAIL, payload: response.data });
};

export const createRecipe = formValues => async dispatch => {
    try {
        const response = await recipes.post('/recipes/', { ...formValues });
        dispatch({ type: CREATE_RECIPE, payload: response.data });
        return response;
    } catch (error) {
        dispatch({ type: ERROR, payload: error.response.data.message });
    }
};

export const deleteRecipe = id => async dispatch => {
    const response = await recipes.delete(`/recipes/${id}`);

    dispatch({ type: DELETE_RECIPE, payload: id });

    return response;
};
