import {
    FETCH_RECIPE_LIST,
    CREATE_RECIPE,
    DELETE_RECIPE
} from '../actions/types'

export default (state={}, action) => {
    switch (action.type) {
        case FETCH_RECIPE_LIST:
            return action.payload;
        case CREATE_RECIPE:
            return action.payload;
        case DELETE_RECIPE:
            return action.payload;
        default:
            return state;
    }
}
