import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import recipesReducer from "./recipesReducer";
import currentRecipeReducer from "./currentRecipeReducer";
import errorReducer from "./errorReducer";

const reducers = combineReducers({
    form: formReducer,
    recipes: recipesReducer,
    currentRecipe: currentRecipeReducer,
    error: errorReducer,
});

export default reducers;
